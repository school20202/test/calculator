package app;

import org.junit.jupiter.api.*;

public class CalculatorTest {

    private Calculator calculator = new Calculator();


    @Test
    public void testSum(){
        int expected = 20;
        int actual = calculator.sum(10, 10);
        Assertions.assertEquals(expected, actual);
    }

    @AfterEach
    public void destroy(){
        calculator = null;
    }
}
